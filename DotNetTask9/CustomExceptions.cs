﻿using System;

public class ChaosArrayFullException : Exception
{
    public ChaosArrayFullException()
    {
    }

    public ChaosArrayFullException(string message)
        : base(message)
    {
    }

    public ChaosArrayFullException(string message, Exception inner)
        : base(message, inner)
    {
    }
}

public class ChaosSpaceOccupiedException : Exception
{
    public ChaosSpaceOccupiedException()
    {
    }

    public ChaosSpaceOccupiedException(string message)
        : base(message)
    {
    }

    public ChaosSpaceOccupiedException(string message, Exception inner)
        : base(message, inner)
    {
    }
}

public class NoRetrievableItemsException : Exception
{
    public NoRetrievableItemsException()
    {
    }

    public NoRetrievableItemsException(string message)
        : base(message)
    {
    }

    public NoRetrievableItemsException(string message, Exception inner)
        : base(message, inner)
    {
    }
}

public class EmptyChaosSpaceException : Exception
{
    public EmptyChaosSpaceException()
    {
    }

    public EmptyChaosSpaceException(string message)
        : base(message)
    {
    }

    public EmptyChaosSpaceException(string message, Exception inner)
        : base(message, inner)
    {
    }
}