﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask9
{
    class ChaosArray<T>
    {
        private T[] chaosArray;
        private bool [] indexOccupied;
        private int count;


        public ChaosArray()
        {
            chaosArray = new T[10];
            indexOccupied = new bool[10];
            count = 0;
        }
        

        public void Add(T item)
        { 
            if (count >= 10)
            {
                throw new ChaosArrayFullException("The Chaos Array has reached maximum capacity.");
            } 
            else
            {
                PlaceItem(item);
            }
        }
         
        private void PlaceItem(T item)
        {
            Random r; 
            int chaosIndex;
            bool added = false;
            while (!added)
            {
                r = new Random();
                chaosIndex = r.Next(1, 9);
               
                // checking if the specified place is occupied.
                if (indexOccupied[chaosIndex])
                {
                    throw new ChaosSpaceOccupiedException($"The insertion point already has a value at index {chaosIndex}.");

                }
                else // if not, add the item.
                {
                    chaosArray[chaosIndex] = item;
                    count++;
                    added = true;
                    indexOccupied[chaosIndex] = true;

                }
            }
        }
        public T Retreive()
        {
            // if the array is empty there is nothing to retrieve.
            if (count <= 0)
            {
                throw new NoRetrievableItemsException("The Chaos array is empty and has no retrievable items.");
            }
            else
            {
                Random r;
                int chaosIndex;
                while (true)
                {
                    r = new Random();
                    chaosIndex = r.Next(0, 9);
                    // checking if the specified place contains a value.
                    if(indexOccupied[chaosIndex])
                    {
                        return chaosArray[chaosIndex];
                    }
                    else 
                    {
                        throw new EmptyChaosSpaceException($"The Chaos array has no value at the specified index {chaosIndex} to retrieve");

                    }
                }
                
            }
        }

    }
}
