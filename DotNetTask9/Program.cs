﻿using System;

namespace DotNetTask9
{
    class Program
    {
        static void Main(string[] args)
        {
            ChaosArray<int> chaosInts = new ChaosArray<int>();
            ChaosArray<string> chaosStrings = new ChaosArray<string>();
            ChaosArray<bool> chaosBools = new ChaosArray<bool>();

            
            Random r = new Random();
            for (int i = 0; i < 5; i++)
            {
                try
                {
                        chaosInts.Add(r.Next(0, 9));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            try 
            {
                chaosStrings.Add("Hello");
                chaosStrings.Add("OK");
                chaosStrings.Add("Chaos");
                chaosStrings.Add("Bolvar");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            try
            {
            Console.WriteLine($"Retrieved {chaosInts.Retreive()} from chaosInts.");
            Console.WriteLine($"Retrieved {chaosStrings.Retreive()} from chaosStrings.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
